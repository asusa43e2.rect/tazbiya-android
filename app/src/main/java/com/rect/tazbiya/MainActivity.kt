package com.rect.tazbiya

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.View.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var webView: WebView
    private var doubleBackToExitPressedOnce = false

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webView = findViewById(R.id.webview)

        loadMyWeb()

        refresh_layout.setOnRefreshListener {
            loadMyWeb()
            refresh_layout.isRefreshing = false
        }
    }

    private fun loadMyWeb(){
        webView.webViewClient = WebViewClient()
        webView.loadUrl("https://tazbiya.com/")
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                pb_loading_line.visibility = VISIBLE
                view?.loadUrl(url)

                return true
            }

            override fun onPageFinished(view: WebView, weburl: String) {
                pb_loading_line.visibility = GONE
            }

            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
                webView.loadUrl("about:blank")
                Toast.makeText(applicationContext, "Oops Maaf terjadi kesalahan, swipe up atau cek jaringan anda!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            if (doubleBackToExitPressedOnce) {
                dialogAction()
            }
            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click once again to exit", Toast.LENGTH_SHORT).show()
            Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    private fun dialogAction() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Info")
        builder.setMessage("Are you sure you want to close this app?")
        builder.setPositiveButton("YES"){dialog, which ->
            finish()
        }
        builder.setNegativeButton("NO"){dialog, which ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
